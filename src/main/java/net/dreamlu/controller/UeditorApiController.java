package net.dreamlu.controller;

import com.baidu.ueditor.ActionEnter;
import com.jfinal.core.Controller;

/**
 * Ueditor编辑器
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date: 2016年1月9日 上午22:15:13
 */
public class UeditorApiController extends Controller {

	public void index() {
		String outText = ActionEnter.me().exec(getRequest());

		renderHtml(outText);
	}

}
